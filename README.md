#  Hackers vs. Security: Attack-Defence Trees as Asynchronous Multi-Agent Systems

This repository hosts the results for the paper.

## Clone this repository:
```
git clone https://depot.lipn.univ-paris13.fr/parties/publications/transformation-amas.git && cd transformation-amas
```

## Folder Structure
```
.
├── adtrees               # folder with the ADTree models
│   ├── forestall         # forestalling a software release
│   ├── gain-admin        # obtain admin privileges
│   ├── iot-dev           # compromise IoT device
│   ├── scaling           # model showing the impact of different agents configurations
│   └── treasure-hunters  # steal a treasure in a museum
├── eamas                 # folder with the EAMAS transformations (UPPAAL, Imitator & Tikz files)
├── results               # folder with the table of results
└── tool                  # folder with the binaries of the tool adt2amas
```

## Models

### Forestalling a software release (forestall)

This model is based on a real-world instance [1]. It models an attack
to the intellectual property of a company C, by an unlawful competitor
company U aiming at being “first to the market.” Following [2],
software extraction from C takes place before U builds it into its own
product, and U must deploy to market before C, which takes place after U has
integrated the stolen software into its product.

![forestall ADTree](adtrees/forestall/forestall.png)

### Obtain admin privileges (gain-admin)

To gain administrative privileges in a UNIX system, an attacker needs either
physical access to an already logged-in console or remote access via
privilege escalation (attacking SysAdmin). This case study [3] exhibits a
mostly branching structure: all gates but one are disjunctions in the
original tree of [3]. We enrich this scenario with the SAND gates of [2],
and further add reactive defences.

![gain-admin ADTree](adtrees/gain-admin/gain-admin.png)

### Compromise IoT device (iot-dev)

This model describes an attack to an Internet-of-Things (IoT) device either
via wireless or wired LAN. Once the attacker gains access to the private
network and has acquired the corresponding credentials, it can exploit a
software vulnerability in the IoT device to run a malicious script. Our
ADTree adds defence nodes on top of the attack trees used in [4].

![iot-dev ADTree](adtrees/iot-dev/iot-dev.png)

### Treasure Hunters

It models thieves that try to steal a treasure in a museum. To achieve their
goal, they first must access the treasure room, which involves bribing a
guard (b), and forcing the secure door (f). Both actions are costly and take
some time. Two coalitions are possible: either a single thief has to carry
out both actions, or a second thief could be hired to parallelise b and f.
After these actions succeed the attacker/s can steal the treasure (ST), which
takes a little time for opening its display stand and putting it in a bag. If
the two-thieves coalition is used, we encode in ST an extra 90 € to hire the
second thief — the computation function of the gate can handle this plurality
— else ST incurs no extra cost. Then the thieves are ready to flee (TF),
choosing an escape route to get away (GA): this can be a spectacular escape
in a helicopter (h), or a mundane one via the emergency exit (e). The
helicopter is expensive but fast while the emergency exit is slower but at no
cost. Furthermore, the time to perform a successful escape could depend on
the number of agents involved in the robbery. Again, this can be encoded via
computation functions in gate GA.

As soon as the treasure room is penetrated (i.e. after b and f but before ST)
an alarm goes off at the police station, so while the thieves flee the police
hurries to intervene (p). The treasure is then successfully stolen iff the
thieves have fled and the police failed to arrive or does so too late. This
last possibility is captured by the condition associated with the treasure
stolen gate (TS), which states that the arrival time of the police must be
greater than the time for the thieves to steal the treasure and go away.

![Treasure Hunters ADTree](adtrees/treasure-hunters/treasure-hunters.png)


## EAMAS Transformations

In the following, we present the transformation of the `treasure hunters`
ADTree model into its EAMAS model. The reader can find the transformations of
the other models in the folder `eamas`.

![EAMAS treasure hunters](eamas/treasure-hunters/treasure_hunters.png)

## Results

The `EAMAS` models resulting of the transformation can be found in the
`eamas` folder. The reader can found the sources of the `adt2amas` tool
[here](https://depot.lipn.univ-paris13.fr/parties/tools/adt2amas/-/tree/v1.0.0)
or the binaries in the `tool` folder. Below we show some quantitative results.

![Results table](results/results.png)


## Scaling Assignment of Agents

The following model was used for studying the impact of different agent
configurations on the minimum attack time. All nodes in the ADTree were
assigned the same duration, namely 1 time slot. The results are shown on the
graph at its right side, both for an optimal assignment of agents and the
worst-case assignment (which leads to the maximal minimum time).


<img alt="Scaling assignment of agents" src="adtrees/scaling/scaling.png" width="1000">

These experiments assess the importance of agents distribution over the
ADTree nodes, as shown by the difference between the optimal and worst-case
scenarios. These two coincide when there is either a single agent or one
agent per node in the tree, since in both cases there is only a unique
possible assignment. When considering one agent per node, parallelism is
maximal, thus leading to the optimal time for the attack to happen: 5 time
units for 15 agents. However, we observe that this optimality can be achieved
with a lesser number of agents, namely 6. In other words, the fastest
successful attack requires the participation of 6 attackers, with the
appropriate assignment. This assignment can be identified using our approach.

## Authors

- Jaime Arias (LIPN, CNRS UMR 7030, Université Sorbonne Paris Nord)
- Carlos E. Budde (Formal Methods and Tools, University of Twente)
- Wojciech Penczek (Institute of Computer Science, PAS, Warsaw University of Technology)
- Laure Petrucci (LIPN, CNRS UMR 7030, Université Sorbonne Paris Nord)
- Teofil Sidoruk (Institute of Computer Science, PAS, Warsaw University of Technology)
- Mariëlle Stoelinga (Formal Methods and Tools, University of Twente)

## Abstract

Attack-Defence Trees (ADTrees) are a well-suited formalism to assess possible
attacks to systems and the efficiency of counter-measures. This paper extends
the available ADTree constructs with reactive patterns that cover further
security scenarios, and equips all constructs with attributes such as time
and cost to allow for quantitative analyses. We model ADTrees as (an
extension of) Asynchronous Multi-Agents Systems — EAMAS. The ADTree–EAMAS
transformation allows us to quantify the impact of different agents
configurations on metrics such as attack time. Using EAMAS also permits
parametric verification: we derive constraints for property satisfaction,
e.g. the max time a defence can take to block an attack. Our approach is
exercised on several case studies using the Uppaal and IMITATOR tools. We
developed the tool
[adt2amas](https://depot.lipn.univ-paris13.fr/parties/tools/adt2amas)
implementing our transformation.


## References

[1] A. Buldas, P. Laud, J. Priisalu, M. Saarepera, and J. Willemson. Rational
Choice of Security Measures Via Multi-parameter Attack Trees. In Critical
Information Infrastructures Security, pages 235–248. Springer, 2006.

[2] R. Kumar, E. Ruijters, and M. Stoelinga. Quantitative attack tree analysis
via priced timed automata. In FORMATS 2015, volume 9268 of LNCS, pages 156–
171. Springer, 2015.

[3] J. D. Weiss. A system security engineering process. In Proceedings of the
14th National Computer Security Conference, pages 572–581, 1991.

[4] R. Kumar, S. Schivo, E. Ruijters, B. M. Yildiz, D. Huistra, J. Brandt, A.
Rensink, and M. Stoelinga. Effective analysis of attack trees: A model-driven
approach. In Fundamental Approaches to Software Engineering, pages 56–73.
Springer, 2018.
