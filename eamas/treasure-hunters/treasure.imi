(************************************************************
 *                      IMITATOR MODEL
 *
 * Treasure Hunters Attack Defence Tree
 *
 * Description     : Attack Defence Tree
 * Author          : XXX XXX
 *
 * Created         : 2018/11/15
 * Last modified   : 2019/08/20
 *
 * IMITATOR version: 2.10.1
 ************************************************************)

var

(* constants *)
	init_cost_f = 100, init_cost_b = 500, init_cost_h = 500,
	init_cost_e = 0, init_cost_p = 100, init_cost_ST = 0,
	init_cost_GA = 0, init_cost_TF = 0,
	init_time_f = 120, init_time_b = 60, init_time_h = 3,
	init_time_p = 10, init_time_e = 10, init_time_ST = 2,
	init_time_GA = 0, init_time_TF = 0
		: constant;

(* discrete variables *)
	cost_ST, cost_GA, cost_TF, cost_TS,
	time_ST, time_GA, time_TF, time_TS
		: discrete;


(******************)
automaton f
(******************)
synclabs : f_ok, f_nok;
loc l0 : while True wait {}
	when True sync f_ok goto lok;
	when True sync f_nok goto lnok;
loc lok : while True wait {}
loc lnok : while True wait {}
end

(******************)
automaton b
(******************)
synclabs : b_ok, b_nok;
loc l0 : while True wait {}
	when True sync b_ok goto lok;
	when True sync b_nok goto lnok;
loc lok : while True wait {}
loc lnok : while True wait {}
end

(******************)
automaton h
(******************)
synclabs : h_ok, h_nok;
loc l0 : while True wait {}
	when True sync h_ok goto lok;
	when True sync h_nok goto lnok;
loc lok : while True wait {}
loc lnok : while True wait {}
end

(******************)
automaton e
(******************)
synclabs : e_ok, e_nok;
loc l0 : while True wait {}
	when True sync e_ok goto lok;
	when True sync e_nok goto lnok;
loc lok : while True wait {}
loc lnok : while True wait {}
end

(******************)
automaton p
(******************)
synclabs : p_ok, p_nok;
loc l0 : while True wait {}
	when True sync p_ok goto lok;
	when True sync p_nok goto lnok;
loc lok : while True wait {}
loc lnok : while True wait {}
end

(******************)
automaton ST
(******************)
synclabs : ST_ok, ST_nok, b_ok, b_nok, f_ok, f_nok;
loc l0 : while True wait {}
	when True sync b_ok goto l1;
	when True sync b_nok goto l3;
	when True sync f_nok goto l3;
loc l1 : while True wait {}
	when True sync f_ok goto l2;
loc l2 : while True wait {}
	when True sync ST_ok
		do {cost_ST' = init_cost_ST +
				init_cost_b + init_cost_f,
			time_ST' = init_time_ST +
				init_time_b + init_time_f} goto lok;
loc l3 : while True wait {}
	when True sync ST_nok goto lnok;
loc lok : while True wait{}
loc lnok : while True wait{}
end

(******************)
automaton GA
(******************)
synclabs : GA_ok, GA_nok, h_ok, h_nok, e_ok, e_nok;
loc l0 : while True wait {}
	when True sync h_ok
		do {cost_GA' = init_cost_GA + init_cost_h,
			time_GA' = init_time_GA + init_time_h}
		goto l1;
	when True sync e_ok
		do {cost_GA' = init_cost_GA + init_cost_e,
			time_GA' = init_time_GA + init_time_e}
		goto l1;
	when True sync h_nok goto l2;
loc l1 : while True wait {}
	when True sync GA_ok goto lok;
loc l2 : while True wait {}
	when True sync e_nok goto l3;
loc l3 : while True wait {}
	when True sync GA_nok goto lnok;
loc lok : while True wait{}
loc lnok : while True wait{}
end

(******************)
automaton TF
(******************)
synclabs : TF_ok, TF_nok, ST_ok, ST_nok, GA_ok, GA_nok;
loc l0 : while True wait {}
	when True sync ST_ok goto l1;
	when True sync ST_nok goto l3;
loc l1 : while True wait {}
	when True sync GA_ok goto l2;
	when True sync GA_nok goto l3;
loc l2 : while True wait {}
	when True sync TF_ok
		do {cost_TF' = init_cost_TF + cost_ST + cost_GA,
			time_TF' = init_time_TF + time_ST + time_GA} goto lok;
loc l3 : while True wait{}
	when True sync TF_nok goto lnok;
loc lok : while True wait{}
loc lnok : while True wait{}
end

(******************)
automaton TS
(******************)
synclabs : TS_ok, TS_nok, TF_ok, TF_nok, p_ok, p_nok;
loc l0 : while True wait {}
	when True sync TF_ok goto l1;
	when True sync TF_nok goto l3;
	when True sync p_ok goto l3;
loc l1 : while True wait {}
	when True sync p_nok goto l2;
loc l2 : while True wait {}
	when time_GA + init_time_ST < init_time_p sync TS_ok
		do {cost_TS' = cost_TF, time_TS' = time_TF} goto lok;
loc l3 : while True wait {}
	when True sync TS_nok goto lnok;
loc lok : while True wait{}
loc lnok : while True wait{}
end

(*****************)
(* initial state *)
(*****************)
init :=
	(* location *)
	loc[f] = l0 & loc[b]=l0 & loc[h] = l0 & loc[e] = l0 & loc[p] = l0
&	loc[ST] = l0 & loc[GA] = l0 & loc[TF] = l0 & loc[TS] = l0
	(* discrete variables *)
&	cost_ST = 0 & cost_GA = 0 & cost_TF = 0 & cost_TS = 0
&	time_ST = 0 & time_GA = 0 & time_TF = 0 & time_TS = 0
;

(************)
(* property *)
(************)
property := unreachable loc[TS] = lok;
